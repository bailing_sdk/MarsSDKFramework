//
//  BLNavigationController.h
//  MarsSDKFramework_Example
//
//  Created by panfei mao on 2019/1/11.
//  Copyright © 2019 panfei mao. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BLNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
