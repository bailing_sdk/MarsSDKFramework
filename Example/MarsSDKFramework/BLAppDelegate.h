//
//  BLAppDelegate.h
//  MarsSDKFramework
//
//  Created by panfei mao on 11/14/2018.
//  Copyright (c) 2018 panfei mao. All rights reserved.
//

@import UIKit;

@interface BLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
