//
//  main.m
//  MarsSDKFramework
//
//  Created by panfei mao on 11/14/2018.
//  Copyright (c) 2018 panfei mao. All rights reserved.
//

@import UIKit;
#import "BLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BLAppDelegate class]));
    }
}
