//
//  BLViewController.m
//  MarsSDKFramework
//
//  Created by panfei mao on 11/14/2018.
//  Copyright (c) 2018 panfei mao. All rights reserved.
//

#import "BLViewController.h"
#import <MarsFramework/MarsFramework.h>

#define kChannelName @"YiCYX_geren"
#define kMarsWebUrl  @"https://th5.bailingpay.com/blf/index?jumpFrom=02"

@interface BLViewController () <MarsWebViewDelegate>

@property (nonatomic, strong) UITextField   *urlTextField;
@property (nonatomic, strong) UIButton      *demoButton;
@property (nonatomic, strong) UIButton      *loginButton;
@property (nonatomic, strong) UIButton      *choiseUserButton;

@property (nonatomic, strong) UILabel       *userInfoLabel;

@end

@implementation BLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.title = @"MarsWebDemo";
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGFloat viewWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat textFieldX = 20.0;
    
    UITextField *userIdTextField = [[UITextField alloc] init];
    userIdTextField.placeholder = @"地址URL";
    userIdTextField.frame = CGRectMake(textFieldX, 100.0, viewWidth - 40.0, 40.0);
    userIdTextField.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:userIdTextField];
    self.urlTextField = userIdTextField;
    self.urlTextField.text = kMarsWebUrl;
    self.urlTextField.text = @"https://th53.bailingpay.com/blhome/index?returnData=eyJqdW1wRnJvbSI6IjAyIiwiYXBwVHlwZSI6IkFQUCIsInRoaXJkQmFja1VybCI6Imh0dHBzOlwvXC9jZG4uNWljaG9uZy5jb21cL3N1Y2Nlc3MuaHRtbD9ibGluZyJ9&marsInfo=eyJtb2JpbGUiOiIxNTMwNTc5NzA5OCIsInByb2plY3RDb2RlIjoiU0hCVF9BaUNob25nX3lvbmdodSIsInByb2plY3RVc2VySWQiOjk2MDMsInNpZ24iOiIzYTJiY2ZkYmEzOGNiMWY1YTUyNThiMTBjODIzZGNjOCIsInByb2plY3RVc2VyVG9rZW4iOiIzN2NjYWRjOTA5ODY2NmFlMWY4OGZhZTJjN2I0MTQ0MiJ9&sdkInfo=ewogICJzZGtWZXJzaW9uIiA6ICIyMzEiCn0=";
//    self.urlTextField.text = @"https://yh5.bailingpay.com/blhome/index?returnData=eyJqdW1wRnJvbSI6IjAyIiwiYXBwVHlwZSI6IkFQUCIsInRoaXJkQmFja1VybCI6Imh0dHA6Ly9iZ2subm1tODAuY29tIn0=&marsInfo=eyJtb2JpbGUiOiIxNTMwNTc5NzA5OCIsInByb2plY3RDb2RlIjoiQ0dCVF9YUFhfeW9uZ2h1IiwicHJvamVjdFVzZXJJZCI6IjE0ODU1OCIsInByb2plY3RVc2VyVG9rZW4iOiI0YWQyYjc4NmE4ZWYxZDdiZDc4ZTBjZGUwMTc3N2U5YyIsInNpZ24iOiI0NTZmYmYyOGQ4NGUwMDUzYWFhNTM3MjE1ZjhiNzk1NyJ9&sdkInfo=ewogICJzZGtWZXJzaW9uIiA6ICIyMzEiCn0=";
    self.urlTextField.text = @"https://th52.bailingpay.com/blloan/index?returnData=eyJqdW1wRnJvbSI6IjAxIiwiYXBwVHlwZSI6IkFQUCIsInRoaXJkQmFja1VybCI6Imh0dHA6Ly9iZ2subm1tODAuY29tIn0%3D&marsInfo=eyJtb2JpbGUiOiIxNTI2ODE4OTMzOCIsInByb2plY3RDb2RlIjoiQ0dCVF9YUFhfeW9uZ2h1IiwicHJvamVjdFVzZXJJZCI6IjEwMDAwNCIsInByb2plY3RVc2VyVG9rZW4iOiJjMmZhZjA2ODQyMmEwNTcxNzlmN2VlYzk1NjRmOWE2ZSIsInNpZ24iOiI0NWU2YWNkZjMwYjBiMzRjMTNjNDIzOTllNWFlOGVhMSJ9";
//    self.urlTextField.text = @"https://th51.bailingpay.com/blhome/sdkprotocol";//协议测试
    
    CGFloat demoButtonW = 160.0;
    CGFloat demoButtonH = 50.0;
    CGFloat loginButtonX = CGRectGetMinX(self.urlTextField.frame);
    CGFloat buttonMargin = (viewWidth - loginButtonX * 2 - demoButtonW * 2) / 2.0;
    CGFloat loginButtonY = CGRectGetMaxY(self.urlTextField.frame) + 50.0;
    
    
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.loginButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.loginButton setTitle:@"登录" forState:UIControlStateNormal];
    self.loginButton.layer.borderWidth = 1.0;
    self.loginButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.loginButton.layer.cornerRadius = 4.0;
    self.loginButton.frame = CGRectMake(loginButtonX, loginButtonY, demoButtonW, demoButtonH);
    [self.view addSubview:self.loginButton];
    [self.loginButton addTarget:self action:@selector(loginButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.demoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.demoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.demoButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.demoButton setTitle:@"Mars 测试入口" forState:UIControlStateNormal];
    self.demoButton.layer.borderWidth = 1.0;
    self.demoButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.demoButton.layer.cornerRadius = 4.0;
    self.demoButton.frame = CGRectMake(CGRectGetMaxX(self.loginButton.frame) + buttonMargin, CGRectGetMinY(self.loginButton.frame), demoButtonW, demoButtonH);
    [self.view addSubview:self.demoButton];
    [self.demoButton addTarget:self action:@selector(demoButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.choiseUserButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.choiseUserButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.choiseUserButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.choiseUserButton setTitle:@"选择登录用户" forState:UIControlStateNormal];
    self.choiseUserButton.layer.borderWidth = 1.0;
    self.choiseUserButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.choiseUserButton.layer.cornerRadius = 4.0;
    self.choiseUserButton.frame = CGRectMake(loginButtonX, CGRectGetMaxY(self.loginButton.frame) + 50.0, demoButtonW, demoButtonH);
    //    [self.view addSubview:self.choiseUserButton];
    [self.choiseUserButton addTarget:self action:@selector(choiseUserButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.userInfoLabel = [[UILabel alloc] init];
    self.userInfoLabel.font = [UIFont systemFontOfSize:18];
    self.userInfoLabel.numberOfLines = 0;
    self.userInfoLabel.frame = CGRectMake(CGRectGetMinX(self.choiseUserButton.frame), CGRectGetMaxY(self.choiseUserButton.frame) + 50.0, CGRectGetWidth(self.urlTextField.frame), 150.0);
    [self.view addSubview:self.userInfoLabel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:@"loginSuccess" object:nil];
    
    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)loginButtonAction{
    NSLog(@"点击登录");
}

- (void)demoButtonAction{
    if (self.urlTextField.text.length <= 0) {
        return;
    }
    //  跳转到web 页面
    MarsWebViewController *marsWebVC = [[MarsWebViewController alloc] init];
    marsWebVC.webUrlStr = self.urlTextField.text;
    marsWebVC.delegate = self;
    [self.navigationController pushViewController:marsWebVC animated:YES];
}

/** 返回第三方APP首页 */
- (void)marsReturnHomePage{
    NSLog(@"返回首页");
}
/** 退出MarsWebViewController */
- (void)marsWebViewExit{
    NSLog(@"退出");
}

/** 支付结果 resultCode=1 支付成功， resultCode=0 支付失败 */
- (void)marsPayResultWithVC:(MarsWebViewController *)marsVC andCode:(NSString *)resultCode resultParam:(NSString *)paramString{
    //
    [marsVC.navigationController popViewControllerAnimated:YES];
    //  根据code做相应的跳转处理
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
