#
# Be sure to run `pod lib lint MarsSDKFramework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MarsSDKFramework'
  s.version          = '2.4.5'
  s.summary          = 'MarsSDKFramework for cocoaPods'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'MarsSDKFramework for ios. 用于cocoaPods引入'

  s.homepage         = 'https://gitee.com/bailing_sdk/MarsSDKFramework'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'panfei mao' => '843158788@qq.com' }
  s.source           = { :git => 'https://gitee.com/bailing_sdk/MarsSDKFramework.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = "MarsSDKFramework", "MarsSDKFramework/**/*.{h,m}"
  s.libraries     = 'z' #表示依赖的系统类库，比如libz.dylib等(iOS lib库省略lib前缀)
  s.frameworks    = 'UIKit','Foundation'
  
  s.vendored_frameworks = "MarsSDKFramework/*.{framework}"
  s.resource_bundle = {
      'MarsFramework' => ['MarsSDKFramework/Resources/*.png']
  }
  s.requires_arc = true
  #s.vendored_libraries = 'MarsSDKFramework/MarsFramework.framework'
  #s.static_framework = true
  
  # s.resource_bundles = {
  #   'MarsSDKFramework' => ['MarsSDKFramework/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
