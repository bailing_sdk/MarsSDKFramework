//
//  MarsWebViewController.h
//  WKWebDemo
//
//  Created by yangpenghua on 2018/3/27.
//  Copyright © 2018年 linzi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MarsWebViewController;

@protocol MarsWebViewDelegate <NSObject>
@optional
/** 弹出第三方登陆 */
- (void)marsWebViewUserLogin;
/** 返回第三方APP首页 */
- (void)marsReturnHomePage;
/**
 支付结果

 @param marsVC MarsSDK控制器，进行三方自己的跳转
 @param resultCode 支付结果 resultCode=1 支付成功， resultCode=0 支付失败
 @param resultData 传入三方的参数列表
 */
- (void)marsPayResultWithVC:(MarsWebViewController *)marsVC andCode:(NSString *)resultCode resultParam:(NSDictionary *)resultData;
/** 退出MarsWebViewController */
- (void)marsWebViewExit;

/** 设置导航栏背景颜色 */
- (UIColor *)marsSetNavBarBackgroundColor;
/** 设置导航栏背景图片 */
- (UIImage *)marsSetNavBarBackgroundImage;

@end

@interface MarsWebViewController : UIViewController
/** 加载URL字符串 */
@property (nonatomic, copy) NSString    *webUrlStr;
/** 进度条颜色 */
@property (nonatomic, strong) UIColor   *progressColor;
/** Mars代理 */
@property (nonatomic, weak) id<MarsWebViewDelegate> delegate;

@end
