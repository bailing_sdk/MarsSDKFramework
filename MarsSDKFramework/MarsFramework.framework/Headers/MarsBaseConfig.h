//
//  MarsBaseConfig.h
//  MarsWebDemo
//
//  Created by yangpenghua on 2018/3/28.
//  Copyright © 2018年 linzi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarsBaseConfig : NSObject

///  Return a shared config object.
+ (MarsBaseConfig *)sharedConfig;

///  配置用户的渠道
@property (nonatomic, strong) NSString *channel;
///  配置用户的平台的userId 唯一标识
@property (nonatomic, strong) NSString *userId;
///  配置用户的注册手机号
@property (nonatomic, strong) NSString *userPhone;
///  配置用户签名
@property (nonatomic, strong) NSString *sign;
///  配置用户token
@property (nonatomic, strong) NSString *userToken;

/**
 *  用户登录时调用此方法
 *
 *  @param userId    用户ID,字母,数字,下划线或@符号
 *  @param userPhone  字母,数字,下划线,汉字或@符号
 *  @param userSign  字母,数字,下划线,汉字或@符号
 *  @param userToken 字母,数字,下划线,汉字或@符号
 *
 */
- (void)loginWithUserId:(NSString *)userId
           andUserPhone:(NSString *)userPhone
                andSign:(NSString *)userSign andToken:(NSString *)userToken;

/**
 *  用户退出登陆方法
 */
- (void)loginOut;

@end
